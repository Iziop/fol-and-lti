package hsostarter.js

import colibri.Subject
import cats.data.ValidatedNel
import cats.effect.SyncIO
import cats.syntax.apply._
import colibri.Observer.sink
import colibri.{Observable, Sink}
import fastparse.Parsed
import outwatch.{VDomModifier, VNode}
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import hsostarter.js.Editable.ops._
import hsostarter.js.IndexedFormula.toPrenex
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom
import org.scalajs.dom.XMLHttpRequest

import scala.scalajs.js

final case class TwoFormula(firstFormula: String, secondFormula: String)

object TwoFormula {
  def httpGet(theUrl: String): String = {
    val xmlHttp = new XMLHttpRequest()
    xmlHttp.open("GET", theUrl)
    xmlHttp.send(null)
    dom.window.alert("Отправлен запрос!")

    // val json = JSON.parse(xmlHttp.responseText)
    // val json = js.JSON.parse(xmlHttp.responseText)
    // (json \ 1 \ "value").asInstanceOf[String]
    xmlHttp.responseText.substring(10, 16)
    // xmlHttp.responseText
  }
  val twoformulaPresentable: Presentable[TwoFormula] =
    Presentable(twoformula =>
      span(s"${twoformula.firstFormula} ${twoformula.secondFormula}"),
    )

  implicit val twoformulaEditable: Editable[TwoFormula] =
    twoformula =>
      for {
        firstNameEditor <- twoformula.firstFormula.editor
        lastNameEditor  <- twoformula.secondFormula.editor
      } yield
        new Editor[TwoFormula] {

          def result: Observable[ValidatedNel[String, TwoFormula]] =
            Observable.combineLatestMap(
              firstNameEditor.result,
              lastNameEditor.result,
            )(
              (
                firstNameValidated,
                lastNameValidated,
              ) =>
                (firstNameValidated, lastNameValidated).mapN {
                  var res: String = ""
                  (f1, f2) =>
                    val (l, r) = (
                      FormulaParser.Parse(f1),
                      FormulaParser.Parse(f2),
                    )
                    res = httpGet(
                      s"http://127.0.0.1:8090/compare?for1=$f1&for2=$f2",
                    )
                    TwoFormula.apply(res, "")

                },
            )

          def isnormal2: Observable[ValidatedNel[String, TwoFormula]] =
            Observable.combineLatestMap(
              firstNameEditor.result,
              lastNameEditor.result,
            )(
              (
                firstNameValidated,
                lastNameValidated,
              ) =>
                (firstNameValidated, lastNameValidated).mapN(TwoFormula.apply),
            )
          def present: VDomModifier =
            div(
              div("Формула 1: ", twoformula.firstFormula),
              div("Формула 2:", lastNameEditor.present),
            )

          override def ispnf(
            x: String,
          ): Observable[ValidatedNel[String, TwoFormula]] = ???
        }

}
