package hsostarter.js
import cats.data.ValidatedNel
import colibri.Observable
import fastparse.Parsed
import hsostarter.js.Editable.ops._
import org.scalajs.dom
import org.scalajs.dom.{fetch, XMLHttpRequest}
import outwatch.VDomModifier
import outwatch.dsl._

import java.time.LocalDateTime
import scala.scalajs.js
import scala.scalajs.js.JSON

final case class Person(firstName: String)

object Person {
  def httpGet(theUrl: String): String = {
    val xmlHttp = new XMLHttpRequest()
    xmlHttp.open("GET", theUrl)
    xmlHttp.send(null)
    dom.window.alert("Отправлен запрос!")

    // val json = JSON.parse(xmlHttp.responseText)
    // val json = js.JSON.parse(xmlHttp.responseText)
    // (json \ 1 \ "value").asInstanceOf[String]
    xmlHttp.responseText.substring(10, 16)
    // xmlHttp.responseText
  }

  def httpPostScore(score: Int) = {

    val xmlHttp = new XMLHttpRequest()

    xmlHttp.open("POST", "http://127.0.0.1:8090/scores")

    // xmlHttp.setRequestHeader("Authorization", "Bearer " + token)
    // val time = LocalDateTime.now().toString
    val data =
      s"""
        |{
        |  "score":$score
        |}
        |
        |""".stripMargin

    xmlHttp.send(score)

  }

  val personPresentable: Presentable[Person] =
    Presentable(person => span(s"${person.firstName} ${person.firstName}"))

  implicit val personEditable: Editable[Person] =
    person =>
      for {
        firstNameEditor <- person.firstName.editor

      } yield
        new Editor[Person] {

          def result: Observable[ValidatedNel[String, Person]] =
            firstNameEditor.result.map(
              _.map {
                var res: String = ""

                x =>
                  val parse_result: Parsed[Formula] = FormulaParser.Parse(x)
                  parse_result match {
                    case Parsed.Success(value, _) =>
                      res = " is " + IndexedFormula
                        .toPrenex(Formula.toIndexed(value)._1)

                    case failure: Parsed.Failure =>
                      res = "Incorrect formula, error:\n" + failure.trace()
                  }

                  Person("Prenex of " + x + res)

              },
            )

          """def result: Observable[ValidatedNel[String, Person]] =
            firstNameEditor.result.map(
              _.map(x => Person("Prenex of " + x + res)),
            )"""
          def ispnf(for1: String): Observable[ValidatedNel[String, Person]] =
            firstNameEditor.result.map(
              _.map {
                var res: String = ""
                // val client = FetchClientBuilder[IO].create

                x =>
                  val p = FormulaParser.Parse(for1)
                  p match {
                    case Parsed.Success(value, _) =>
                      // val pnf = IndexedFormula
                      // .toPrenex(Formula.toIndexed(value)._1)
                      // val cond = pnf.toString == x
                      // if (cond) res = "Right!"
                      // else res = "Wrong!"
                      // val response = client
                      //  .expect[String](
                      //    s"http://localhost:8080/ispnf?value=$x",
                      //  ).map(resp => res)
                      // val responseText =
                      //  dom.fetch(s"http://localhost:8080/ispnf?value=$x")

                      res = httpGet(
                        s"http://127.0.0.1:8090/ispnf?answer=$x&randfor=$for1",
                      )

                      httpPostScore(
                        80,
                      )

                    // res = responseText
                    // res = responseText
                    case failure: Parsed.Failure =>
                      res = "Incorrect formula, error:\n" + failure.trace()
                  }
                  Person(res)

              },
            )
          def present: VDomModifier =
            div(
              div("Формула:", firstNameEditor.present),
            )

          override def isnormal2: Observable[ValidatedNel[String, Person]] = ???
        }
}
