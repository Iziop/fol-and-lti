package hsostarter.jvm

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

import hsostarter.jvm.data.db.DbSchema

object DbSchemaDumper {

  def main(args: Array[String]): Unit = {
    val schemaStr = DbSchema.createStatementsList.mkString(";\n\n")
    val schemaBytes = schemaStr.getBytes(StandardCharsets.UTF_8)
    val path = Paths.get("schema.sql")
    Files.write(path, schemaBytes)
    println(s"created $path")
  }

}
