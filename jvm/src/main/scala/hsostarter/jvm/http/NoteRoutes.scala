package hsostarter.jvm.http

import cats.syntax.functor._
import cats.syntax.flatMap._
import cats.effect.Sync
import hsostarter.jvm.data.db.{NoteDBIO, Transactor}
import hsostarter.jvm.models
import hsostarter.jvm.models.{Note, NoteData}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import mouse.string._

object NoteIdVar {
  def unapply(str: String): Option[Note.Id] =
    str.parseIntOption.map(models.Id.apply[Note, Int])
}

final class NoteRoutes[F[_]: Sync](
  transactor: Transactor[F],
) extends Http4sDsl[F] {

  def routes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "notes" / NoteIdVar(noteId) =>
        for {
          notes  <- transactor.transact(NoteDBIO.byId(noteId))
          result <- Ok(notes)
        } yield result
      case request @ POST -> Root / "notes" =>
        for {
          note   <- request.as[NoteData]
          id     <- transactor.transact(NoteDBIO.insert(note))
          result <- Ok(id)
        } yield result
      case request @ PUT -> Root / "notes" =>
        for {
          note   <- request.as[Note]
          _      <- transactor.transact(NoteDBIO.update(note))
          result <- Ok()
        } yield result
      case DELETE -> Root / "notes" / NoteIdVar(noteId) =>
        for {
          _      <- transactor.transact(NoteDBIO.delete(noteId))
          result <- Ok()
        } yield result
    }

}
